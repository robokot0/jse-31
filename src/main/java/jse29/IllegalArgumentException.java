package jse29;

/**
 * Неправильное значение аргумента
 */
public class IllegalArgumentException extends Exception {
    /**
     * Констрктор по умолчанию
     * @param message текст сообщения
     */
    IllegalArgumentException(String message){
        super(message);
    }
}
