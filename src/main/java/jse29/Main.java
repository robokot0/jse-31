package jse29;

import java.util.Scanner;

public class Main {
    NumberService numberService = new NumberService();

    NumberController numberController = new NumberController(numberService);

    public static void main(String[] args) throws IllegalArgumentException {
        final Scanner scanner = new Scanner(System.in);
        final Main app = new Main();
        String command = "";
        app.numberController.displayWelcome();
        while (!TerminalConst.EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }

    }
    /**
     * Выполнение комманды
     * @param param Команда
     * @return -1 ошибка 0 Выполнено
     */
    public int run(final String param) {
        try {
            if (param == null || param.isEmpty()) {
                return -1;
            }
            switch (param) {
                case TerminalConst.SUM: return numberController.calcSum();
                case TerminalConst.FACTORIAL: return numberController.calcFactorial();
                case TerminalConst.FIBONACCCI: return numberController.decomposeFibonacci();
                case TerminalConst.HELP: return numberController.displayHelp();
                case TerminalConst.EXIT: return 0;
                default:
                    return numberController.displayError();
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

}
