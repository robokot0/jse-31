package jse29;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class NumberServiceTest {
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());

    @Test
    /**
     * Проверка преобразования строки в число на небольшом числе
     */
    void convertStringToBigInteger1PositivValue() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(BigInteger.valueOf(4),numberService.convertStringToBigInteger("4"));
    }
    @Test
    /**
     * Проверка преобразования строки в число на отрицательном числе
     */
    void convertStringToBigIntegerNegativValue() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(BigInteger.valueOf(-4),numberService.convertStringToBigInteger("-4"));
    }
    @Test
    /**
     * Проверка преобразования строки в число на Long.MAX_VALUE
     */
    void convertStringToBigIntegerLongMax() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(BigInteger.valueOf(Long.MAX_VALUE),numberService.convertStringToBigInteger(Long.valueOf(Long.MAX_VALUE).toString()));
    }
    @Test
    /**
     * Проверка преобразования строки в число на Long.MIN_VALUE
     */
    void convertStringToBigIntegerLongMin() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(BigInteger.valueOf(Long.MIN_VALUE),numberService.convertStringToBigInteger(Long.valueOf(Long.MIN_VALUE).toString()));
    }
    @Test
    /**
     * Проверка преобразования не числа
     */
    void convertStringToBigIntegerNotNumberArg() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.convertStringToBigInteger("aaaa"));
    }
    @Test
    /**
     * Проверка что BigInteger может быть преобразовано в long для небольшого числа
     */
    void testBigIntegerAsLongValue() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(4L,numberService.testBigIntegerAsLong(BigInteger.valueOf(4)));
    }
    @Test
    /**
     * Проверка что BigInteger не может быть преобразовано в long для большего чем Long.MAX_VALUE
     */
    void testBigIntegerAsLongMaxLong() {
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.testBigIntegerAsLong(BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.valueOf(10))));
    }
    @Test
    /**
     * Проверка что BigInteger не может быть преобразовано в long для меньшего чем Long.MIN_VALUE
     */
    void testBigIntegerAsLongMinLong() {
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.testBigIntegerAsLong((BigInteger.valueOf(Long.MIN_VALUE).add(BigInteger.valueOf(-10)))));
    }

    @Test
    /**
     * Проверка суммирвоания двух небольших чисел
     */
    void calcSum() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(4,numberService.calcSum("1","3"));
    }

    @Test
    /**
     * Проверка первый аругмент sum не число
     */
    void sumExceptionArg1NotNumber(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum("aaa","111"));
    }
    @Test
    /**
     * Проверка второй аругмент sum не число
     */
    void sumExceptionArg2NotNumber(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum("111","фффф"));
    }
    @Test
    /**
     * Проверка ошибка переполнения при суммирвоании Long.MAX_VALUE
     */
    void sumExceptionOverflowMaxLong(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum(Long.valueOf(Long.MAX_VALUE).toString() ,"10"));
    }
    @Test
    /**
     * Проверка ошибка переполнения при суммирвоании Long.MIN_VALUE
     */
    void sumExceptionOverflowMinLong(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.calcSum(Long.valueOf(Long.MIN_VALUE).toString() ,"-10"));
    }
    @Test
    /**
     * Проверка ошибка при попытке вычислить факториал ототрицательного числа
     */
    void factorialNegativeValue(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.calcFactorial("-10"));
    }
    @Test
    /**
     * Проверка ошибка при попытке вычислить факториал небольшого числа
     */
    void factorial() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(2432902008176640000L,numberService.calcFactorial("20"));
    }

    @Test
    /**
     * Факториал 0
     */
    void factorial0() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(1L,numberService.calcFactorial("0"));
    }

    @Test
    /**
     * Факториал 1
     */
    void factorial1() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(1L,numberService.calcFactorial("1"));
    }

    @Test
    /**
     * Факториал 2
     */
    void factorial2() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(2L,numberService.calcFactorial("2"));
    }

    @Test
    /**
     * Факториал 4
     */
    void factorial4() throws IllegalArgumentException {
        NumberService numberService = new NumberService();
        assertEquals(24L,numberService.calcFactorial("4"));
    }

    @Test
    /**
     * Факториал переполнение long
     */
    void factorialNegativeOverflow(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.calcFactorial("21"));
    }
    @Test
    /**
     * Факториал не числовой аргумент
     */
    void fibonacciNoNumberArg(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.decomposeFibonacci("aaa"));
    }
    @Test
    /**
     * Разложение на числа Фиббоначи ошибка на отрицательный аргумент
     */
    void fibonacciNegativeArg(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.decomposeFibonacci("-1"));
    }
    @Test
    /**
     * Разложение 0 на числа Фиббоначи ошибка
     */
    void fibonacciZeroArg(){
        NumberService numberService = new NumberService();
        assertThrows(IllegalArgumentException.class,() -> numberService.decomposeFibonacci("0"));
    }

    @Test
    /**
     * Разложение небольшого числа на слагаемые проверка что сумма слагаемых осталась равна числу
     */
    void decomposeFibonacci() throws IllegalArgumentException {
        NumberService numberService = new NumberService();

        String strResult = "8252";

        long[] result = numberService.decomposeFibonacci(strResult);

        long sumResult = 0l;
        for(int i=0;i<result.length;i++) {
            logger.info(result[i]);
            sumResult=sumResult+result[i];
        }

        assertEquals(Long.valueOf(strResult).longValue(),sumResult);
    }

    @Test
    /**
     * Расчет списка чисел Фибоначи
     * в списке должен быть ряд Фибоначи
     */
    void calcFibonacci() throws IllegalArgumentException {
        NumberService numberService = new NumberService();

        long longArg = 150;

        ArrayList<Long> resultForCheck = numberService.calcFibonacci(longArg);

        assertEquals(1l,resultForCheck.get(0).longValue());
        assertEquals(1l,resultForCheck.get(1).longValue());

        for(int i=2;i<resultForCheck.size();i++) {
            assertEquals(resultForCheck.get(i-1).longValue()+resultForCheck.get(i-2).longValue(),resultForCheck.get(i).longValue());
        }

        logger.info(resultForCheck);
    }
    @Test
    /**
     * Расчет списка чисел Фибоначи
     * последний элемент должен быть меньшге или равен аргументу
     */
    void calcFibonacciLastMemberLeArg() throws IllegalArgumentException {
        NumberService numberService = new NumberService();

        long longArg = 150;

        ArrayList<Long> resultForCheck = numberService.calcFibonacci(longArg);

        assertEquals(true,resultForCheck.get(resultForCheck.size()-1).longValue()<=longArg);
    }
}